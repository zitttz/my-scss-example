import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import {TournamentModule} from './tournament/tournament.module';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    TournamentModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
