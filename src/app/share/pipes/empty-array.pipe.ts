import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'emptyArray'
})
export class EmptyArrayPipe implements PipeTransform {
  transform(value): number[] {
    const res = [];
    for (let i = 0; i < value; i++) {
      res.push(i);
    }
    return res;
  }
}
