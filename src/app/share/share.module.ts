import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EmptyArrayPipe } from './pipes/empty-array.pipe';



@NgModule({
  declarations: [EmptyArrayPipe],
  exports: [
    EmptyArrayPipe,
  ],
  imports: [
    CommonModule,
  ],
})
export class ShareModule { }
