import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MatchHolderComponent } from './match-holder.component';

describe('MatchHolderComponent', () => {
  let component: MatchHolderComponent;
  let fixture: ComponentFixture<MatchHolderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MatchHolderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MatchHolderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
