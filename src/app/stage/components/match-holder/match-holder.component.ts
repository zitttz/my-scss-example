import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-match-holder',
  templateUrl: './match-holder.component.html',
  styleUrls: ['./match-holder.component.scss']
})
export class MatchHolderComponent implements OnInit {

  @Input() name: number;

  constructor() { }

  ngOnInit() {
  }

}
