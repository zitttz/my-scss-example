import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-opponent',
  templateUrl: './opponent.component.html',
  styleUrls: ['./opponent.component.scss']
})
export class OpponentComponent implements OnInit {

  @Input() name: string;

  constructor() { }

  ngOnInit() {
  }

}
