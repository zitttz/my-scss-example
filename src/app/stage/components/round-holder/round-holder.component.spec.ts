import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RoundHolderComponent } from './round-holder.component';

describe('RoundHolderComponent', () => {
  let component: RoundHolderComponent;
  let fixture: ComponentFixture<RoundHolderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RoundHolderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RoundHolderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
