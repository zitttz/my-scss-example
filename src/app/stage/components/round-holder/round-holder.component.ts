import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-round-holder',
  templateUrl: './round-holder.component.html',
  styleUrls: ['./round-holder.component.scss']
})
export class RoundHolderComponent implements OnInit {

  @Input() name: number;

  constructor() { }

  ngOnInit() {
  }

}
