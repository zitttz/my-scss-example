import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ConnectorsComponent } from './components/connectors/connectors.component';
import { ConnectorComponent } from './components/connectors/connector/connector.component';
import { RoundHolderComponent } from './components/round-holder/round-holder.component';
import { MatchHolderComponent } from './components/match-holder/match-holder.component';
import { OpponentComponent } from './components/match-holder/opponent/opponent.component';
import { StageComponent } from './stage/stage.component';
import {ShareModule} from '../share/share.module';



@NgModule({
  declarations: [ConnectorsComponent, ConnectorComponent, RoundHolderComponent, MatchHolderComponent, OpponentComponent, StageComponent],
  exports: [
    StageComponent,
  ],
  imports: [
    CommonModule,
    ShareModule,
  ],
})
export class StageModule { }
