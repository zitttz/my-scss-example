import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-stage',
  templateUrl: './stage.component.html',
  styleUrls: ['./stage.component.scss']
})
export class StageComponent implements OnInit {

  @Input() name: string;

  constructor() { }

  ngOnInit() {
  }

}
