import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TournamentComponent } from './tournament/tournament.component';
import {StageModule} from '../stage/stage.module';



@NgModule({
  declarations: [TournamentComponent],
  imports: [
    CommonModule,
    StageModule,
  ],
  exports: [
    TournamentComponent,
  ],
})
export class TournamentModule { }
